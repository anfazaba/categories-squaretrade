package com.squartrade.categories.service;

import com.squartrade.categories.dto.CategoryKeywordDto;
import com.squartrade.categories.entity.Category;
import com.squartrade.categories.entity.Keyword;
import com.squartrade.categories.repository.CategoryRepository;
import com.squartrade.categories.repository.KeywordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final KeywordRepository keywordRepository;

    public List<Category> getCategories () {
        return categoryRepository.findAll();
    }

    public List<Category> postCategories (List<Category> categoryList) {
        return categoryRepository.saveAll(categoryList);
    }

    public List<Category> postCategoriesKeywords(List<CategoryKeywordDto> categoryKeywordDtoList) {
        for (CategoryKeywordDto categoryKeywordDto: categoryKeywordDtoList) {
            if(categoryKeywordDto.getName()!= null) {
                Category category = categoryRepository.findByNameIgnoreCase(categoryKeywordDto.getName());
                if(category == null) {
                    category = new Category();
                    category.setName(categoryKeywordDto.getName());
                }

                if(categoryKeywordDto.getSubcategories() != null) {
                    List<Category> subcategories = categoryKeywordDto.getSubcategories().stream()
                            .map(subcatName -> {
                                Category subcategory = categoryRepository.findByNameIgnoreCase(subcatName);
                                if (subcategory == null) {
                                    subcategory = new Category();
                                    subcategory.setName(subcatName);
                                }
                                return subcategory;
                            })
                            .collect(Collectors.toList());
                    category.setSubcategories(subcategories);
                }

                if(categoryKeywordDto.getKeywords() != null) {
                    List<Keyword> keywords = categoryKeywordDto.getKeywords().stream()
                            .map(keywordName -> {
                                Keyword keyword = keywordRepository.findByNameIgnoreCase(keywordName);
                                if (keyword == null) {
                                    keyword = new Keyword();
                                    keyword.setName(keywordName);
                                }
                                return keyword;
                            })
                            .collect(Collectors.toList());
                    category.setKeywords(keywords);
                }

                categoryRepository.save(category);
            }
        }
        return categoryRepository.findAll();
    }
    public void deleteCategories (List<Long> categories) {
        categoryRepository.deleteAllById(categories);
    }

}
