package com.squartrade.categories.service;

import com.squartrade.categories.dto.KeywordDeleteDto;
import com.squartrade.categories.entity.Keyword;
import com.squartrade.categories.mapper.KeywordMapper;
import com.squartrade.categories.repository.KeywordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class KeywordService {

    private final KeywordRepository keywordRepository;
    private final KeywordMapper keywordMapper;

    public List<Keyword> getKeywords () {
        return keywordRepository.findAll();
    }

    public List<Keyword> postKeywords (List<Keyword> keywordList) {
        return keywordRepository.saveAll(keywordList);
    }

    public void deleteKeywords (List<KeywordDeleteDto> keywordList) {
        keywordRepository.deleteAll(keywordMapper.keywordDeleteDtoListToKeywordList(keywordList));
    }
}
