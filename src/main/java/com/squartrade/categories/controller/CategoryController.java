package com.squartrade.categories.controller;

import com.squartrade.categories.dto.CategoryKeywordDto;
import com.squartrade.categories.entity.Category;
import com.squartrade.categories.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Category> getCategories() {
        return categoryService.getCategories();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<Category> postCategories(@RequestBody List<Category> categories) {
        return categoryService.postCategories(categories);
    }

    @PostMapping("/keywords")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Category> postCategoryKeyword(@RequestBody List<CategoryKeywordDto> categoryKeywordDtoList) {
        return categoryService.postCategoriesKeywords(categoryKeywordDtoList);
    }


    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategories(@RequestBody List<Long> categoriesId) {
        categoryService.deleteCategories(categoriesId);
    }
}
