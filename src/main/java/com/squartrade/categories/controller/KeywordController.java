package com.squartrade.categories.controller;

import com.squartrade.categories.dto.KeywordDeleteDto;
import com.squartrade.categories.dto.KeywordDto;
import com.squartrade.categories.entity.Category;
import com.squartrade.categories.entity.Keyword;
import com.squartrade.categories.service.KeywordService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/keywords")
@RequiredArgsConstructor
public class KeywordController {

    private final KeywordService keywordService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Keyword> getKeywords() {
        return keywordService.getKeywords();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<Keyword> postKeywords(@RequestBody List<Keyword> keywords) {
        return keywordService.postKeywords(keywords);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteKeywords(@RequestBody List<KeywordDeleteDto> keywords) {
       keywordService.deleteKeywords(keywords);
    }

}
