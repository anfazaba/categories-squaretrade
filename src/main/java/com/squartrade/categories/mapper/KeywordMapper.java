package com.squartrade.categories.mapper;

import com.squartrade.categories.dto.KeywordDeleteDto;
import com.squartrade.categories.entity.Keyword;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KeywordMapper {

    List<Keyword> keywordDeleteDtoListToKeywordList (List<KeywordDeleteDto> keywordDeleteDtoList);

}
