package com.squartrade.categories.dto;

import lombok.Data;

@Data
public class KeywordDto {

    public Long id;
    public String name;
}
