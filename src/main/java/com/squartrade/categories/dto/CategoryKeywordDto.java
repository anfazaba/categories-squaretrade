package com.squartrade.categories.dto;

import lombok.Data;

import java.util.List;

@Data
public class CategoryKeywordDto {

    private String name;
    private List<String> subcategories;
    private List<String> keywords;

}
