package com.squartrade.categories.dto;

import lombok.Data;

@Data
public class KeywordDeleteDto {

    private Long id;
}
