package com.squartrade.categories.repository;

import com.squartrade.categories.entity.Keyword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeywordRepository extends JpaRepository<Keyword, Long> {
    Keyword findByNameIgnoreCase(String keywordName);

}
